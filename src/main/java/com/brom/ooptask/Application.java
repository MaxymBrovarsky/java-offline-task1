package com.brom.ooptask;

import com.brom.ooptask.constants.StringConstant;
import com.brom.ooptask.controller.TransportationController;
import com.brom.ooptask.menu.Menu;
import com.brom.ooptask.view.TransportationView;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {
    private Menu menu;
    private TransportationView transportationView;
    private TransportationController transportationController;
    public static void main(String[] args) {
        ApplicationContext context = new GenericXmlApplicationContext("config.xml");
        Application application = context.getBean("application", Application.class);
        application.handleInput();
    }

    public void handleInput() {
        Scanner scanner = new Scanner(System.in);
        this.showMenu();
        while (true) {
            try {
                int commandIndex = scanner.nextInt();
                scanner.nextLine();
                this.menu.executeCommand(commandIndex);
                this.showMenu();
            } catch (InputMismatchException e) {
                System.out.println(StringConstant.WRONG_INPUT_MESSAGE);
                scanner.nextLine();
            }
        }
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public void setTransportationView(TransportationView transportationView) {
        this.transportationView = transportationView;
    }

    public void showMenu() {
        this.transportationView.showMenu(menu);
    }

    public TransportationView getTransportationView() {
        return transportationView;
    }

    public TransportationController getTransportationController() {
        return transportationController;
    }

    public void setTransportationController(TransportationController transportationController) {
        this.transportationController = transportationController;
    }
}
