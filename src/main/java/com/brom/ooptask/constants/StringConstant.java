package com.brom.ooptask.constants;

public class StringConstant {
    public static final String ENTER_MAX_PRICE_MESSAGE = "Please enter max price for filtering";
    public static final String ENTER_FROM_CITY_MESSAGE =
            "Please enter name of city, from which transportation will departure";
    public static final String ENTER_TO_CITY_MESSAGE =
            "Please enter name of city to which transportation will come";
    public static final String ENTER_TONNES_MESSAGE =
            "Please enter value of transport weight in tonnes";
    public static final String WRONG_INPUT_MESSAGE = "You have entered wrong data, please try again";
    public static final String MENU_TILE = "Menu:";
}
