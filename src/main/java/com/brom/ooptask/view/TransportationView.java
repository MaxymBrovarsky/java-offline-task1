package com.brom.ooptask.view;

import com.brom.ooptask.menu.Menu;
import com.brom.ooptask.model.Transportation;

import java.util.List;

public class TransportationView {
    public void showTransportation(List<Transportation> transportation) {
        transportation.forEach(System.out::println);
    }
    public void showMenu(Menu menu) {
        System.out.println(menu);
    }
}
