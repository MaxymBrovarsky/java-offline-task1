package com.brom.ooptask.model;

import java.time.LocalDate;
import java.time.Period;

public class Transportation {
    private String sourceCity;
    private String destinationCity;
    private int weightInTonnes;
    private double price;
    private LocalDate departureDate;
    private LocalDate arrivalDate;

    public Transportation(String sourceCity, String destinationCity, int weightInTonnes,
                          double price, LocalDate departureDate, LocalDate arrivalDate) {
        this.sourceCity = sourceCity;
        this.destinationCity = destinationCity;
        this.weightInTonnes = weightInTonnes;
        this.price = price;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public String getSourceCity() {
        return sourceCity;
    }

    public void setSourceCity(String sourceCity) {
        this.sourceCity = sourceCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public int getWeightInTonnes() {
        return weightInTonnes;
    }

    public void setWeightInTonnes(int weightInTonnes) {
        this.weightInTonnes = weightInTonnes;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getDurationInDays() {
        Period period = Period.between(this.arrivalDate, this.departureDate);
        int diff = period.getDays();
        return diff;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" + this.sourceCity + "->" + this.destinationCity +
                " price: " + this.price +" weight:" + this.weightInTonnes + " departure date: " +
                this.departureDate.toString() + " arrival date: " + this.arrivalDate.toString();
    }
}
