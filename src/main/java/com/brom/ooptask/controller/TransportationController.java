package com.brom.ooptask.controller;

import com.brom.ooptask.model.Transportation;
import com.brom.ooptask.repository.TransportationRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TransportationController {
    private TransportationRepository transportationRepository;
    private List<Transportation> currentTransportation;

    public List<Transportation> getTransportation() {
        this.currentTransportation = this.transportationRepository.getTransportation();
        return currentTransportation;
    }

    public List<Transportation> getTransportationWithPriceLowerThan(double maxPrice) {
        this.currentTransportation = this.transportationRepository.findTransportationWithPriceLowerThan(maxPrice);
        return currentTransportation;
    }

    public List<Transportation> findByFromCityAndToCityAndTonnesValue(String fromCity, String toCity, int tonnes) {
        this.currentTransportation =
                this.transportationRepository.findByFromCityAndToCityAndTonnesValue(fromCity, toCity, tonnes);
        return currentTransportation;
    }

    public void sortCurrentTransportationAscByPrice() {
        Comparator<Transportation> priceComparator = Comparator.comparingDouble(Transportation::getPrice);
        Collections.sort(this.currentTransportation, priceComparator);
    }


    public void sortCurrentTransportationAscByDuration() {
        Comparator<Transportation> durationComparator = Comparator.comparingInt(Transportation::getDurationInDays);
        Collections.sort(this.currentTransportation, durationComparator);
    }

    public void setTransportationRepository(TransportationRepository transportationRepository) {
        this.transportationRepository = transportationRepository;
    }

    public List<Transportation> getCurrentTransportation() {
        return currentTransportation;
    }
}
