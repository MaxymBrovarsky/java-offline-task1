package com.brom.ooptask.menu.commands;


import com.brom.ooptask.controller.TransportationController;
import com.brom.ooptask.model.Transportation;

import java.util.List;

public class SortAscByPriceCommand extends Command {
    private static String title = "Sort by price";
    @Override
    public void execute() {
        TransportationController transportationController = application.getTransportationController();
        transportationController.sortCurrentTransportationAscByPrice();
        List<Transportation> transportation = transportationController.getCurrentTransportation();
        this.application.getTransportationView().showTransportation(transportation);
    }

    @Override
    public String toString() {
        return title;
    }
}
