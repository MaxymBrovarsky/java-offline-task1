package com.brom.ooptask.menu.commands;

public class QuitCommand extends Command {
    private static String title = "Quit";
    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String toString() {
        return title;
    }
}
