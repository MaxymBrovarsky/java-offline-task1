package com.brom.ooptask.menu.commands;

import com.brom.ooptask.constants.StringConstant;
import com.brom.ooptask.model.Transportation;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class FilterByMaxPriceCommand extends Command{
    private static String title = "Show transportation with price lower than ?";
    @Override
    public void execute() {
        try{
            Scanner scanner = new Scanner(System.in);
            System.out.println(StringConstant.ENTER_MAX_PRICE_MESSAGE);
            double maxPrice = scanner.nextDouble();
            scanner.nextLine();
            List<Transportation> transportation =
                    this.application.getTransportationController().getTransportationWithPriceLowerThan(maxPrice);
            this.application.getTransportationView().showTransportation(transportation);
        } catch (InputMismatchException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return title;
    }
}
