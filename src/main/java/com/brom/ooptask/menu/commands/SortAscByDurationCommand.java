package com.brom.ooptask.menu.commands;

import com.brom.ooptask.controller.TransportationController;

public class SortAscByDurationCommand extends Command {
    private static String title = "Sort by duration";
    @Override
    public void execute() {
        TransportationController transportationController = application.getTransportationController();
        transportationController.sortCurrentTransportationAscByDuration();
        application.getTransportationView().showTransportation(transportationController.getCurrentTransportation());
    }

    @Override
    public String toString() {
        return title;
    }
}
