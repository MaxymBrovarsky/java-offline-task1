package com.brom.ooptask.menu.commands;

import com.brom.ooptask.Application;
import com.brom.ooptask.controller.TransportationController;

public abstract class Command {
    protected Application application;

    public void setApplication(Application application) {
        this.application = application;
    }

    public abstract void execute();

    public abstract String toString();

}
