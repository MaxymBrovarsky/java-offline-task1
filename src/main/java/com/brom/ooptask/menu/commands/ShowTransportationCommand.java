package com.brom.ooptask.menu.commands;

import com.brom.ooptask.constants.StringConstant;
import com.brom.ooptask.controller.TransportationController;
import com.brom.ooptask.model.Transportation;

import java.util.List;
import java.util.Scanner;

public class ShowTransportationCommand extends Command {
    private static String title = "Show transportation from ? to ?";
    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(StringConstant.ENTER_FROM_CITY_MESSAGE);
        String fromCity = scanner.nextLine();
        System.out.println(StringConstant.ENTER_TO_CITY_MESSAGE);
        String toCity = scanner.nextLine();
        System.out.println(StringConstant.ENTER_TONNES_MESSAGE);
        int tonnes = scanner.nextInt();
//        scanner.nextLine();
        TransportationController transportationController = application.getTransportationController();
        List<Transportation> filtered =
                transportationController.findByFromCityAndToCityAndTonnesValue(fromCity, toCity, tonnes);
        application.getTransportationView().showTransportation(filtered);

    }

    @Override
    public String toString() {
        return title;
    }
}
