package com.brom.ooptask.menu;

import com.brom.ooptask.constants.StringConstant;
import com.brom.ooptask.menu.commands.Command;

import java.util.List;

public class Menu {
    private static int offsetFromZero = 1;
    private List<Command> commands;

    public List<Command> getCommands() {
        return commands;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    public void executeCommand(int index) {
        if (index < offsetFromZero || index > this.commands.size()) {
            return;
        }
        this.commands.get(index - offsetFromZero).execute();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(StringConstant.MENU_TILE + "\n");
        for (int i = 0; i < commands.size(); i++) {
            sb.append(i + offsetFromZero + ". " + commands.get(i) + ";\n");
        }
        return sb.toString();
    }
}
