package com.brom.ooptask.repository;

import com.brom.ooptask.model.Transportation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TransportationRepository {
    private List<Transportation> transportation;

    public TransportationRepository() {
        this.transportation = new ArrayList<>();
        transportation.add(
                new Transportation("Odesa", "Marsel", 10, 1000,
                        LocalDate.now().plusDays(2), LocalDate.now().plusDays(8))
        );
        transportation.add(
                new Transportation("Odesa", "Marsel", 10, 5000,
                        LocalDate.now().plusDays(3), LocalDate.now().plusDays(4))
        );
        transportation.add(
                new Transportation("Odesa", "Marsel", 11, 2000,
                        LocalDate.now().plusDays(4), LocalDate.now().plusDays(7))
        );
        transportation.add(
                new Transportation("Odesa", "Marsel", 20, 3000,
                        LocalDate.now().plusDays(1), LocalDate.now().plusDays(9))
        );
        transportation.add(
                new Transportation("Odesa", "Marsel", 60, 5000,
                        LocalDate.now().plusDays(2), LocalDate.now().plusDays(8))
        );

    }

    public List<Transportation> getTransportation() {
        return transportation;
    }

    public void setTransportation(List<Transportation> transportation) {
        this.transportation = transportation;
    }

    public List<Transportation> findTransportationWithPriceLowerThan(double maxPrice) {
        List<Transportation> filtered = transportation.stream()
                .filter(currentTransportation -> currentTransportation.getPrice() <= maxPrice)
                .collect(Collectors.toList());
        return filtered;
    }

    public List<Transportation> findByFromCityAndToCityAndTonnesValue(String fromCity, String toCity, int tonnes) {
        List<Transportation> filtered = transportation.stream()
                .filter(t -> {
                    return (t.getSourceCity().equals(fromCity) &&
                            t.getDestinationCity().equals(toCity) &&
                            t.getWeightInTonnes() == tonnes
                    );
                })
                .collect(Collectors.toList());
        return filtered;
    }
}
